# Angular Moment Picker

## Installation

Get Angular Moment Picker from [**npm**](https://www.npmjs.com/), [**bower**](http://bower.io/) or [**git**](https://git-scm.com/):
```
 Add "moment-datepicker": "https://bitbucket.org/gamechangesns/moment-datepicker.git" to bower.json
bower install

add material UI css in main.scss
@import "bower_components/moment-datepicker/dist/themes/material-ui.min.css";

```

Add *moment-picker* dependency to your module:
```js
var myApp = angular.module('myApp', ['moment-picker']);
```

Provide the attribute to your element:
```html
<div moment-picker="myDate"> {{ myDate }} </div>
```

## Options

To configure Angular Moment Picker you have to add to your element or your input the attribute relative to the options you want to set.

```html
<div moment-picker="ctrl.birthday" locale="fr" format="LL">
    Mon anniversaire est le {{ ctrl.birthday }}
</div>
```

```html
<input moment-picker="ctrl.dateFormatted" ng-model="ctrl.momentDate" format="DD/MM/YYYY">
```
